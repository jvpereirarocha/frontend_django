from django.shortcuts import render
import datetime


def index(request):
    year = datetime.datetime.now().year
    message = "Sistema de Gerenciamento de Inventário"
    context = {
        'message': message,
        'year': year,
    }
    return render(request, 'apps/index.html', context)


def form(request):
    context = {}
    context['year'] = datetime.datetime.now().year
    return render(request, 'apps/products/new.html', context)


def list(request):
    context = {}
    context['year'] = datetime.datetime.now().year
    return render(request, 'apps/products/list.html', context)
