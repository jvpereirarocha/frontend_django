from django.urls import path
from .views import index, form, list

urlpatterns = [
    path('', index, name='index'),
    path('new', form, name='form'),
    path('list', list, name='list')
]
